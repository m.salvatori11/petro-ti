package com.petro.lancamentonotaservice.repository;

import com.petro.lancamentonotaservice.model.Contrato;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContratoRepository extends JpaRepository<Contrato,Long> {
}
