package com.petro.lancamentonotaservice.model;


import com.petro.lancamentonotaservice.library.BaseBean;
import com.petro.lancamentonotaservice.library.DTOEntity;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="contrato")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@SequenceGenerator(name = "seq_contrato", sequenceName = "seq_contrato",  allocationSize = 10)
public class Contrato extends BaseBean<Long> implements Serializable, DTOEntity {

    private static final long serialVersionUID = -1380542986106975475L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_contrato")
    private Long id;

    @Length(min = 1, max = 200, message = "Nome Contrato deve conter entre 1 e 200  caracteres")
    @Column(name = "nome_contrato",nullable = false)
    private String nomeContrato;

    @Length(min = 1, max = 200, message = "Razao Social deve conter entre 1 e 200  caracteres")
    @Column(name = "razao_social",nullable = false)
    private String descricao;

    @Length(min = 1, max = 200, message = "Nome Responsavel deve conter entre 1 e 200  caracteres")
    @Column(name = "nome_responsavel")
    private String nomeResponsavel;



    @Length(min = 1, max = 200, message = "Email Responsavel deve conter entre 1 e 200  caracteres")
    @Column(name = "email_responsavel")
    private String telefoneResponsavel;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "PI_EXERCICIO",nullable = false)
    private Date dtExercicio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_empresa" , nullable = false)
    private Empresa empresa;

    @Override
    public Long getIdEntity() {
        return  this.id;
    }
}