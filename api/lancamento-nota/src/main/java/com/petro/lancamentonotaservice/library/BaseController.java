package com.petro.lancamentonotaservice.library;



import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;

import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

public abstract class BaseController<E,D extends BaseBean, SS extends BaseService<D, ?, ID>, ID extends Serializable>{

    @Autowired
    private SS service;
    private DTOEntity mapperEntity;
    private DTOEntity mapperDto;
    @Autowired
    protected    PagedResourcesAssembler<E> assembler;

    public void BaseController(DTOEntity mapperEntity,DTOEntity mapperDto){
        this.mapperEntity = mapperEntity;
        this.mapperDto    = mapperDto;
    }

    @GetMapping(value = "/{id}", produces = {"application/json", "application/x-yaml"})
    public E findById(@PathVariable("id") ID id) {
        D d = service.findById(id);
        return convertToDto(d);
    }
    @GetMapping(produces = {"application/json","application/x-yaml"})
    public ResponseEntity<?> findAll(@RequestParam(value = "page", defaultValue = "0") int page,
                                     @RequestParam(value = "limit", defaultValue = "12") int limit) {

        Pageable pageable = PageRequest.of(page,limit);

        var pagemodels =  service.findAll(pageable);
        Page<E> models = pagemodels.map(this::convertToDto);
        PagedModel<EntityModel<E>> pagedModel = assembler.toModel(models);

        return new ResponseEntity<>(pagedModel, HttpStatus.OK);
    }

    @PostMapping(produces = {"application/json","application/x-yaml"},
            consumes = {"application/json","application/x-yaml"})
    public E save(@RequestBody E e) {
        D d = convertToEntity(e);
        d = service.save(d);
        return convertToDto(d);
    }

    @PutMapping(produces = {"application/json","application/x-yaml"},
            consumes = {"application/json","application/x-yaml"})
    public E  update(@RequestBody E e) {
        D d =  convertToEntity(e);
        return   convertToDto((service.update(d,d)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") ID  id) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }

    public E convertToDto(Object obj) {

        return (E) new ModelMapper().map(obj, mapperDto.getClass());
    }

    public  D convertToEntity(Object obj) {

        return (D) new ModelMapper().map(obj, mapperEntity.getClass());
    }


}
