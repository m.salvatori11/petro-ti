package com.petro.lancamentonotaservice.repository;


import com.petro.lancamentonotaservice.model.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpresaRepository extends JpaRepository<Empresa,Long> {
}
