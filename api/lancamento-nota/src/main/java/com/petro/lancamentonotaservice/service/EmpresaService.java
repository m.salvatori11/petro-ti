package com.petro.lancamentonotaservice.service;

import com.petro.lancamentonotaservice.library.BaseService;
import com.petro.lancamentonotaservice.model.Empresa;
import com.petro.lancamentonotaservice.repository.EmpresaRepository;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService extends BaseService<Empresa, EmpresaRepository, Long> {
}
