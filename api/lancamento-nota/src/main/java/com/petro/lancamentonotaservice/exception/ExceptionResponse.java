package com.petro.lancamentonotaservice.exception;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse implements Serializable {

    private static final long serialVersionUID = 7803791385145347485L;
    private Date timeStamp;
    private String message;
    private String details;


}
