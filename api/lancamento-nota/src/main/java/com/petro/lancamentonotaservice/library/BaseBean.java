package com.petro.lancamentonotaservice.library;

import java.io.Serializable;


public class BaseBean<ID> implements Serializable {

	protected ID idEntity;


	public ID getIdEntity() {
		return idEntity;
	}

	public void setIdEntity(ID idEntity) {
		this.idEntity = idEntity;
	}





}
