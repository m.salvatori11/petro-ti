package com.petro.lancamentonotaservice.service;

import com.petro.lancamentonotaservice.library.BaseService;
import com.petro.lancamentonotaservice.model.Contrato;
import com.petro.lancamentonotaservice.repository.ContratoRepository;
import org.springframework.stereotype.Service;

@Service
public class ContratoService  extends BaseService<Contrato, ContratoRepository, Long> {
}
