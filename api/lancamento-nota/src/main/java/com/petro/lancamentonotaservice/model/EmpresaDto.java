package com.petro.lancamentonotaservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.petro.lancamentonotaservice.library.DTOEntity;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;


@JsonPropertyOrder({"id"})
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class EmpresaDto extends RepresentationModel<EmpresaDto> implements Serializable, DTOEntity {
    private static final long serialVersionUID = -3434467267230781680L;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("nomeFantasia")
    private String nomeFantasia;

    @JsonProperty("razaoSocial")
    private String razaoSocial;

    @JsonProperty("cnpj")
    private String cnpj;

    @JsonProperty("nomeUnidade")
    private String nomeUnidade;

    @JsonProperty("cep")
    private String cep;

    @JsonProperty("endereco")
    private String endereco;

    @JsonProperty("telefone1")
    private String telefone1;

    @JsonProperty("telefone2")
    private String telefone2;

    @JsonProperty("telefone")
    private String telefone;

    @JsonProperty("responsavel")
    private String responsavel;

    @JsonProperty("cpfResponsavel")
    private String cpfResponsavel;

    @JsonProperty("emailResponsavel")
    private String emailResponsavel;

    @JsonProperty("estado")
    private String estado;

    @JsonProperty("cidade")
    private String cidade;


}
