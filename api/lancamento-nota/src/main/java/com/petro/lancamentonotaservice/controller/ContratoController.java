package com.petro.lancamentonotaservice.controller;

import com.petro.lancamentonotaservice.library.BaseController;
import com.petro.lancamentonotaservice.model.Contrato;
import com.petro.lancamentonotaservice.model.ContratoDto;
import com.petro.lancamentonotaservice.service.ContratoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/contratos")
public class ContratoController extends BaseController<ContratoDto, Contrato, ContratoService, Long> {

    @PostConstruct
    public void ContratoController(){ super.BaseController(new Contrato(), new ContratoDto());}
}
