package com.petro.lancamentonotaservice.library;



import com.petro.lancamentonotaservice.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;
import java.util.Optional;

public abstract class BaseService<E extends Serializable,  RR extends JpaRepository<E, ID>, ID extends Serializable> {


	@Autowired
	private RR repo;

	public E save(@Validated E item) {return 	repo.save(item);}

	public E findById(ID id) {
		var entity = repo.findById(id)
		.orElseThrow(() -> new ResourceNotFoundException("No Records found for ID"));
		return entity;
	}

	public Page<E> findAll(Pageable pageable) {
		return repo.findAll(pageable);
	}

	public void delete(ID id) {
		var entity = repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No Records found for ID"));
		repo.delete(entity);
	}

	public E update(E e, BaseBean baseBean) {
		ID id = (ID) baseBean.getIdEntity();
		final Optional<E> optionalE = repo.findById(id);
		if (!optionalE.isPresent()) {
			new ResourceNotFoundException("No Records found for ID");
		}
		return  repo.save(e);
	}


}
