CREATE USER petro WITH PASSWORD 'petro@2021';
create schema sch_petro;
GRANT ALL ON SCHEMA sch_petro TO petro;
ALTER TABLESPACE petro_data OWNER TO petro;

select * from pg_tablespace;

CREATE USER potiguar WITH PASSWORD 'potiguar';
CREATE role bi_potiguar;
GRANT select ON public.dim_contacontabil_potiguar_nivel_1 TO bi_potiguar;
GRANT select ON public.dim_contacontabil_potiguar_nivel_2 TO bi_potiguar;
GRANT select ON public.dim_contacontabil_potiguar_nivel_3 TO bi_potiguar;
GRANT select ON public.dim_contacontabil_potiguar_nivel_4 TO bi_potiguar;
GRANT select ON public.dim_contacontabil_potiguar_nivel_5 TO bi_potiguar;
GRANT select ON public.dim_contacontabil_potiguar_nivel_6 TO bi_potiguar;
GRANT select ON public.dim_projeto_potiguar TO bi_potiguar;
GRANT select ON public.dim_date TO bi_potiguar;
GRANT select ON public.dim_centrocusto_potiguar TO bi_potiguar;
GRANT select ON public.dim_campo_potiguar TO bi_potiguar;
GRANT select ON public.fato_contabil_potiguar TO bi_potiguar;
GRANT  bi_potiguar TO potiguar;