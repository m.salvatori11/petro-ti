import { Injector } from '@angular/core';
import { CargoService } from './../shared/cargo.service';
import { Cargo } from './../shared/cargo.model';
import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceForm } from 'src/app/shared/base-resource-form/base-resource-form.abstract';

@Component({
  selector: 'app-cargo-form',
  templateUrl: './cargo-form.component.html',
  styleUrls: ['./cargo-form.component.css']
})
export class CargoFormComponent extends BaseResourceForm<Cargo> {

  constructor(protected cargoService: CargoService, protected injector: Injector) {
    super(injector, new Cargo(), cargoService, Cargo.fromJson);
  }


  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      id: [null],
      descricao: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(150) ]],
      ativo: [true, Validators.required]
    });
  }


  protected creationPageTitle(): string {
    return 'Cadastro de Cargos';
  }

  protected editionPageTitle(): string {
    const custoName = this.resource.descricao || '';
    return 'Editando: ';
  }
}
