import { BaseResourceModel } from "src/app/shared/models/base-resource.model";

export class Cargo extends BaseResourceModel {
  constructor(
    public id?:number,
    public descricao?: string,
    public ativo?: boolean
  ){
    super();
  }


  static fromJson(jsonData: any): Cargo {
    return Object.assign(new Cargo(), jsonData);
  }
}
