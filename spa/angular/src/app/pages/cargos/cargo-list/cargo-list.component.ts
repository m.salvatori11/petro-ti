import { CargoService } from './../shared/cargo.service';
import { Cargo } from './../shared/cargo.model';
import { Component, OnInit } from '@angular/core';
import { BaseResourceList } from 'src/app/shared/base-resource-list/base-resource-list.abstract';

@Component({
  selector: 'app-cargo-list',
  templateUrl: './cargo-list.component.html',
  styleUrls: ['./cargo-list.component.css']
})

  export class CargoListComponent extends BaseResourceList<Cargo> {

    constructor(private cargoService: CargoService) {

      super(cargoService);
    }
}
