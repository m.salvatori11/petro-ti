import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SolicitacoesRoutingModule } from './solicitacoes-routing.module';
import { SolicitacaoListComponent } from './solicitacao-list/solicitacao-list.component';
import { SolicitacaoFormComponent } from './solicitacao-form/solicitacao-form.component';

@NgModule({
  imports: [
    CommonModule,
    SolicitacoesRoutingModule,
    SharedModule
  ],
  declarations: [SolicitacaoListComponent, SolicitacaoFormComponent]
})
export class SolicitacoesModule { }
