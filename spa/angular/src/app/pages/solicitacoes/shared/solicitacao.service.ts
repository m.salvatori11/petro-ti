import { SolicitacaoItem } from './solicitacaoItem.model';
import { Observable } from 'rxjs';
import { Solicitacao } from './solicitacao.model';
import { Injectable, Injector } from '@angular/core';


import { BaseResourceService } from "../../../shared/services/base-resource.service";


@Injectable({
  providedIn: 'root'
})
export class SolicitacaoService extends BaseResourceService<Solicitacao> {

  solicitacoesItens = new Array<SolicitacaoItem>();

  constructor(protected injector: Injector) {

    super("http://localhost:9090/solicitacoes", injector, Solicitacao.fromJson);

  }
   public addSolicitacaoItem(solicitacaoItem:SolicitacaoItem){
    this.solicitacoesItens.push(solicitacaoItem);

   }
   public deleteSolicitacaoItem(i:number) {
      this.solicitacoesItens.splice(i,1);
    }
    create(solicitacao: Solicitacao): Observable<Solicitacao> {
      debugger
      solicitacao.solicitacoesItens = this.solicitacoesItens;
      return  super.create(solicitacao);
    }
  }
