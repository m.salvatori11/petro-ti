import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';
import { SolicitacaoItem } from './solicitacaoItem.model';
export class Solicitacao extends BaseResourceModel {
  constructor(
    public id?:number,
    public numeroSolicitacao?: string,
    public dataSolicitacao?: string,
    public dataPrazo?: string,
    public solicitacoesItens?: Array<SolicitacaoItem>

  ){
    super();
  }


  static fromJson(jsonData: any): Solicitacao {
    return Object.assign(new Solicitacao(), jsonData);
  }


}
