import { CustoService } from './../shared/custo.service';
import { Custo } from './../shared/custo.model';
import { Component, OnInit } from '@angular/core';
import { BaseResourceList } from 'src/app/shared/base-resource-list/base-resource-list.abstract';


@Component({
  selector: 'app-custo-list',
  templateUrl: './custo-list.component.html',
  styleUrls: ['./custo-list.component.css']
})
export class CustoListComponent extends BaseResourceList<Custo> {

  constructor(private custoService: CustoService) {
    super(custoService);
  }

}
