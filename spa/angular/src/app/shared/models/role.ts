import { BaseResourceModel } from './base-resource.model';

export class Role extends BaseResourceModel {
  name: string;
  authority: any;
}