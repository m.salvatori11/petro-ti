import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.scss']
})
export class OrderTableComponent implements OnInit {



  @Input()
  columns: any[];


  resources: any[];

  get items(): any[] {
    return this.resources;
  }

  @Input('items')
  set items(value: any[]) {
    this.resources = value;
  }


  @Input() paginator: boolean;
  @Input() rows: string;
  constructor(
   ) {
  }

  @Output() delete = new EventEmitter<any>();
  
  deleteResource(value: any) {
    this.delete.emit(value);
  }
  ngOnInit() {

  }
  


}
