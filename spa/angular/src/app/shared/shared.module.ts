import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { OrderTableComponent } from './order-table/order-table.component';
import { AuthGuard } from './guard/auth.guard';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { HttpRequestInterceptor } from './_helpers/http-request.interceptor';
import { BreadCrumbComponent } from './bread-crumb/bread-crumb.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { FormFieldErrorComponent } from './form-field-error/form-field-error.component';
import { ServerErrorMessagesComponent } from './server-error-messages/server-error-messages.component';
import { ToastrModule } from 'ngx-toastr';
import {InputMaskModule} from 'primeng/inputmask';
import {DropdownModule} from 'primeng/dropdown';



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    CalendarModule,
    RouterModule,
    InputTextareaModule,
    TableModule,
    DialogModule,
    InputMaskModule,
    DropdownModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    BreadCrumbComponent,
    PageHeaderComponent,
    OrderTableComponent,
    FormFieldErrorComponent,
    ServerErrorMessagesComponent,


  ],
  exports: [
    // shared modules
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    DialogModule,
    TableModule,
    InputMaskModule,
    DropdownModule,


    // shared components
    BreadCrumbComponent,
    PageHeaderComponent,
    OrderTableComponent,
    FormFieldErrorComponent,
    ServerErrorMessagesComponent

  ],
  providers: [
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ],
})
export class SharedModule { }
